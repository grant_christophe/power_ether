import "../stylesheets/style.css";
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract'
import gamble_artifacts from '../../build/contracts/Gamble.json'
var Gamble = contract(gamble_artifacts);
var accounts;
var account;
console.log = function(){}
function stop_spin(ticket)
	{
		document.getElementById("w1").innerHTML = ticket[0];
		document.getElementById("w2").innerHTML = ticket[1];
		document.getElementById("w3").innerHTML = ticket[2];
	}
function announce(ticket)
	{
		setTimeout(function()
			{
				document.getElementById("ball_1").innerHTML = ticket[0];
				document.getElementById("ball_1").setAttribute("class","");
				setTimeout(function()
					{
						document.getElementById("ball_2").innerHTML = ticket[1];
						document.getElementById("ball_2").setAttribute("class","");
						
						setTimeout(function()
							{
								document.getElementById("ball_3").innerHTML = ticket[2];
								document.getElementById("ball_3").setAttribute("class","");
							},250);
					},500);
			},700);				
	}

window.App = {
  user:undefined,
  starting_balance:undefined,
  s_balance:undefined,
  e_balance:undefined,
  ending_balance:undefined,
  watch:undefined,
  winning_ticket: [0,0,0],
  start: function() {
    var self = this;
    Gamble.setProvider(web3.currentProvider);
    web3.eth.getAccounts(function(err, accs) {
      if (err != null) {
        alert("There was an error fetching your accounts.");
        return;
      }

      if (accs.length == 0) {
        alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }

      accounts = accs;
      account = accounts[0];
      self.user = account;
      try
		{
			try{
				Gamble.deployed()
				.then(function(contract)
					{
						contract.allEvents({fromBlock: "latest"}).watch( function(err,succ)
							{
								if(succ && succ.args && succ.args.sold){succ.args.sold = succ.args.sold.toNumber();}
								if(succ && succ.args && succ.args.amount){succ.args.amount = succ.args.amount.toNumber();}
								if(succ && succ.args && succ.args.balance){succ.args.balance = succ.args.balance.toNumber() * 0.000000000000000001;}
								if (succ.args.text == "Winner:" || succ.args.text ==  "Service Fee:" || succ.args.text == "Winning numbers set"|| succ.args.text ==  "Rollover:"){succ.args.amount = succ.args.amount * 0.000000000000000001}	
								console.log("Logging:",succ.args);
								try
									{
										if(succ.args && succ.args.text == "Your Old Balance Is:"){self.starting_balance = succ.args.balance;}
										if(succ.args && succ.args.text == "Your New Balance Is:"){self.ending_balance = succ.args.balance;}
										if (succ.args.text == "Winning numbers set")
											{
												stop_spin(succ.args.ticket.split("\n"));
											}
										if (succ.args.text == "Ticket")
											{
												announce(succ.args.ticket.split("\n"));					
											}
										if(succ.args && succ.args.text == "Lost")
											{
												document.getElementById("play").innerHTML = "Play Again";																								
											}
										if(succ.args && succ.args.text == "Winner:")
											{
												alert("!!!CONGRATULATIONS!!!! \nYou just won  $"+succ.args.amount.toFixed(2)+" Ether");
												console.log("Contract Starting:",self.starting_balance,"\nEnding:",self.ending_balance,"\nNet:",self.ending_balance - self.starting_balance);
												self.check();
											}
									}
								catch(e){console.log("Error:",e)}
							});
					})
				.then(function(){console.log("Monitoring Events")}).catch(function(e){console.log("Error:",e)});	
		  }
	  catch(e){console.log("Error:",e)}	
			
		}
	catch(e){console.log("Error:",e)}
		
    });
  },
  check:function()
	  {
		  try
			{	
				Gamble.deployed()
				.then(function(instance){return instance.check_prize.call()})
				.then(function(result){document.getElementById("p_amount").innerHTML = result.toNumber()/1000000000000000000;})
				.catch(function(e){console.log("Error Checking Prize Amount:",e)})
			  }
		  catch(e){console.log("Error Checking Prize Amount:",e)}	
	  },
  winning_ticket:function()
	{
	  var self = this;
	  try
		{
			 Gamble.deployed()
			.then(function(instance){return instance.winning_ticket.call()})
			.then(function(result)
				{
					result = result.split("\n");
					self.winning_ticket = [result[0],result[1],result[2]];
					console.log("Winning Ticket:",self.winning_ticket)
					document.getElementById("w1").innerHTML = result[0];
					document.getElementById("w2").innerHTML = result[1];
					document.getElementById("w3").innerHTML = result[2];
				})
			.catch(function(e){console.log("Error Getting Winning Ticket:",e)})
		}
	  catch(e){console.log("Error Getting Winning Ticket:",e)}	
	},  
  draw:function()
	  {
		  document.getElementById("ball_1").innerHTML = "$";
		  document.getElementById("ball_2").innerHTML = "$";
		  document.getElementById("ball_3").innerHTML = "$";
		  document.getElementById("ball_1").setAttribute("class","roll_up");
		  document.getElementById("ball_2").setAttribute("class","roll_up");
		  document.getElementById("ball_3").setAttribute("class","roll_up");
		  var self = this;
		  try{
				Gamble.deployed()
				.then(function(contract){return contract.draw({from:account,value:(1000000000000000000/50).toString()})})	
				.then(function(result)
					{
						console.log("Draw completed:",result);
						var a = document.createElement("a");
						a.href="https://kovan.etherscan.io/tx/"+result.tx;
						a.innerHTML = new Date();
						document.getElementById("transactions").append(a);
						document.getElementById("transactions").append(document.createElement("br"));						
					})
				.then(function(){self.check()} )
				.catch(function(e){console.log("Error:",e)});	
			  }
		  catch(e){console.log("Error:",e)}	
	  },
};

window.addEventListener('load', function() 
	{
	  if (typeof web3 !== 'undefined') 
		  {
		    console.warn("Using web3 detected from external source.")
		    window.web3 = new Web3(web3.currentProvider);
		  } 
	  App.start();
	  App.check();
	  App.winning_ticket();
	});
