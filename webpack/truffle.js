// Allows us to use ES6 in our migrations and tests.
require('babel-register')

module.exports = {
  networks: {
	live:{
		network_id:1,
		host:"localhost",
		port:8545,
		gas:2338419
	},
	korvan:{
		network_id:42,
		host:"localhost",
		port:8545,
	},
    development: {
      host: 'localhost',
      port: 8545,
      network_id: '*' // Match any network id
    }
  }
}
